# Launchpad
the launchpad sandbox with the JSDoc application and issuelogger plugin already pre-loaded

## Introduction
You can use this template to quickly setup a local launchpad and start developing UI5 applications.

There's 2 ways to do so:
*  You can fork this project from the giturl. This has the advantage that you can always rebase your project whenever the template is updated. The downside is that this means your launchpad becomes a git project, and any apps underneath will have to be setup as sub-modules. This can be a painful process
*  or you can simply download the launchpad project and use it as an expressui5 template. The downside is that your basic launchpad is not automatically updated if the template is, but then again, that's not really a problem, since it's only a local test-launchpad anyway. The upside is that, since it's just a local folder, not connected to git, you can have all your apps and plugins underneath, linked to their own repos without any hassle.

A typical setup for your project will look similar to this structure:
```
Launchpad  
+--appconfig
  +--fioriSandboxConfig.json
+--apps
  +--jsdoco (preloaded component)
+--plugins
  +--issueLogger (preloaded plugin)
+--index.html
```

In the apps folder, you can start new apps with `expressui5` if you select the component template. Same goes for plugins in the plugin folder. (obviously you need to use the `plugin` template).

To make your development easier, you can open the launchpad folder itself, directly in visual code, so that all your apps and plugins are accessible via a single editor instance. It also makes it easier to test your launchpad by simply revving up the live-server.

## Setup
You can install expressui5 via NPM:
`npm install -g expressui5`

next, you need to have the root folder of your npm modules:
`npm root -g`

navigate to that folder and go to 'expressui5/templates'. Copy your templates there.

You can now create a new launchpad in any folder you like by executing the command `epressui5`. *boom* Select the launchpad template, give your project a name and a namespace, and you're good to go.

You can now open it up with Visual Code and test it with liveserver, or NPM. You can also use other editors, or even the webIde, but I like Visual Code.

Every launchpad needs applications. Otherwise it wouldn't be of much use. so you can navigate to the apps folder.

`cd apps`

There you can use expressui5 again to start up a new component.

`expressui5`

Select the component template, give your project a name (folder) and namespace (full namespace domain.projectname). This will generate a new component instance with some basic layouting and controllers.
If you then go in the appconfig folder, you can edit the `fioriSandboxConfig.json` file. In there copy the `jsdoc-display` application entry and paste it underneath (don't forget your comma).
Change the name of your "new" application and change the url to ./apps/`name`/webapp. 

If you then launch your project, you should see a new tile on your launchpad for your new app.

## Testing

When developing the application you can run it locally. There are 2 options:
You can either run the app on node.js by running following command in your terminal:  
```
npm start
```
This command will generate a build version and start a local server. In the meanwhile a watch-task is started for the workspace of the application, so when you adjust a file and perform a save the webpage is reloaded. This way your changes will directly be visible when developing the application.

You can run the application using following url: **[http://localhost:8080/resources/test.html](http://localhost:8080/resources/test.html)**

Or you can choose for an easier option with the visual Code plugin: Live server. This launches a lightweight webserver, and immediately triggers a browser window to open.
You can configure your settings to make it open a Chrome session, in incognito mode, with corse disabled, by adding these configurations in the settings.json.
```
    "liveServer.settings.host": "localhost",    
    "liveServer.settings.AdvanceCustomBrowserCmdLine": "chrome --incognito --remote-debugging-port=9222 --disable-web-security --user-data-dir=c:/temp/chromedev",
```
Live server also injects an auto refresh mechanism, whenever you change anything in your code.

## create new apps / plugins / whatever

You'll notice that the local launchpad project already foresees sub-folders for apps and plugins. Say you want to create a new app. Simply open the app-folder in the terminal, run the command `expressui5`again, and select the component-template. Give your app a name (folder name) and a namespace.

This generates a new subdirectory with a dummy component. To test the component, edit the /appconfig/fioriSandboxConfig.json file of your launchpad, and add your new component as an application.

That is enough to automatically have a new tile on your local launchpad. If you want more possibilities and define intent-based navigation, you can change those settings in the config file as well.

## deploy

You don't deploy your launchpad. It's just there for realistic local testing. You'll have to deploy the individual components. For the how-to on that, please read the instructions in the component, plugin and library templates.

(but basically, it's just `npm install` followed by `npm run deploy` for each component, plugin, library,..)